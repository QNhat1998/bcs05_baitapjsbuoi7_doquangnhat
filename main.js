const getElement = id => document.getElementById(id)

const innerHtml = (id, content) => getElement(id).innerHTML = content

const kiemTraSoNT = nt =>{
    if(nt < 2) return 0;
    for(let i = 2; i <= Math.sqrt(nt);i++) if(nt%i==0) return 0
    return 1
}
let numArray = [], floatArray = []

addArr = () => {
    let n = getElement('arr').value
    n.trim() == '' ? (alert('Chưa nhập số')) : numArray.push(n * 1)
    innerHtml('print', numArray)
    // getElement('arr').value = ''
}

sumPos = () => {
    let sum = 0;
    for (let num of numArray) num > 0 && (sum += num)
    innerHtml('result-sum', `Tổng các số dương trong mảng: ${sum}`)
}

countPos = () => {
    let count = 0;
    for (let num of numArray) num > 0 && ++count
    innerHtml('result-count', `Có ${count} số dương trong mảng`)
}

findMin = () => {
    let min = numArray[0]
    for (let num of numArray) num < min && (min = num)
    innerHtml('result-min', `Số nhỏ nhất trong mảng: ${min}`)
}

findMinPos = () => {
    let newArr = []
    for (let num of numArray) num > 0 && newArr.push(num)
    if (newArr.length > 0) {
        let min = newArr[0]
        for (let num2 of newArr) num2 < min && (min = num2);
        console.log(newArr)
        innerHtml('result-pos', `Số dương nhỏ nhất: ${min}`)
    } else innerHtml('result-pos', 'Không có số nguyên dương nào')

}

findFinalEven = () => {
    let even = 0
    for (let num of numArray) num % 2 == 0 && (even = num)
    innerHtml('result-even', `Số chẵn cuối cùng trong mảng: ${even}`)
}

swap = (index1,index2) =>{
    [numArray[index1], numArray[index2]] = [numArray[index2], numArray[index1]]
}

swapPosition = () => {
    let index1 = getElement('index1').value,
        index2 = getElement('index2').value;
        
    swap(index1,index2)
    innerHtml('result-swap', 'Mảng sau khi đổi: ' + numArray)
}

sortInc = () => innerHtml('result-sort',`${numArray.sort()}`)

findPrime = () =>{
    let index = numArray.findIndex(num => kiemTraSoNT(num) == 1)
    innerHtml('result-prime',numArray[index])
}

addFloat = () => {
    let n = getElement('float').value
    n.trim() == '' ? (alert('Chưa nhập số')) : floatArray.push(n * 1)
    innerHtml('print-float', floatArray)
    // getElement('arr').value = ''
}

countInt = () =>{
    let count = 0
    floatArray.map(item => Number.isInteger(item) && ++count)
    innerHtml('result-int',`Có ${count} số nguyên trong mảng mới`)
}

countNum = () =>{
    let am = 0, duong = 0
    for(let num of numArray)
        num > 0 ? duong++ : num < 0 && am++
    innerHtml('result-ad',`Số dương ${duong>am?'>':duong<am?'<':'='} số âm`)
}


